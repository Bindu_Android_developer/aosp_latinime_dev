package com.android.inputmethod.Manager;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.android.inputmethod.latin.http.HttpManager;


public class NetworkManager
{
    public static final String TAG = "NetworkManager";

    private static NetworkManager instance;

    private final Context mContext;

    private NetworkManager(Context mContext)
    {
        super();
        this.mContext = mContext;
    }

    public static NetworkManager getInstance()
    {
        if (instance == null)
        {
            instance = new NetworkManager(HttpManager.getContext());
        }
        return instance;
    }

    public boolean isOnline(Context context)
    {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = manager.getActiveNetworkInfo();
        return (info != null) && info.isConnectedOrConnecting();
    }

    public static NetworkInfo getAvailableNetwork(Context context)
    {

        NetworkInfo[] infoAvailableNetworks = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getAllNetworkInfo();

        if (infoAvailableNetworks != null)
        {
            for (NetworkInfo network : infoAvailableNetworks)
            {

                if (network.getType() == ConnectivityManager.TYPE_WIFI)
                {
                    if (network.isConnected() && network.isAvailable())
                        return network;
                }
                if (network.getType() == ConnectivityManager.TYPE_MOBILE)
                {
                    if (network.isConnected() && network.isAvailable())
                        return network;
                }
            }
        }

        return null;
    }

    public boolean isConnectedWifi(Context context){
        NetworkInfo info = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);
    }

    public void clearInstance(){
        instance = null;
    }

}
