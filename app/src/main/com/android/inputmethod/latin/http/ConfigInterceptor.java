package com.android.inputmethod.latin.http;

/**
 * Created by bkasmoor on 29/03/18.
 */

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 */
public class ConfigInterceptor implements okhttp3.Interceptor {
  
    private static final String TAG = "ConfigInterceptor";
    private RESPONSE_TYPE type;

    private static final String FILE_EXTENSION = ".json";
    private Context mContext;
    private String mContentType = "application/json";

    public enum RESPONSE_TYPE {XML, JSON}

    public ConfigInterceptor(RESPONSE_TYPE type, Context context) {
        this.type = type;
        this.mContext = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Response response = null;
        ResponseBody body = null;
        String method = chain.request().method().toLowerCase();
        try {
            response = chain.proceed(request);
//            DebugUtil debugUtil = DebugUtil.getInstance();
//            if (debugUtil != null && debugUtil.isLogCatEnabled())
//             {
                String bodyString = response.body().string();
                String strURL = request.url().toString().toLowerCase();
                Log.d(TAG, "Config Request URL " + strURL);
                Log.d(TAG, obfuscateConfig(bodyString));
                 body = ResponseBody.create(response.body().contentType(), bodyString);

//            }
             // Get Request URI.

//            final URI uri = chain.request().url().uri();
//            Log.d(TAG, "--> Request url: [" + method.toUpperCase() + "]" + uri.toString());
//
//            String defaultFileName = getFileName(chain);
//            if (defaultFileName != null) {
//                String fileName = getFilePath(uri, defaultFileName);
//                Log.d(TAG, "Read data from file: " + fileName);
//                InputStream is = mContext.getAssets().open("filters");
//                BufferedReader r = new BufferedReader(new InputStreamReader(is));
//                StringBuilder responseStringBuilder = new StringBuilder();
//                String line;
//                while ((line = r.readLine()) != null) {
//                    responseStringBuilder.append(line).append('\n');
//                    Log.d(TAG, "Response: " + responseStringBuilder.toString());
//                    Response.Builder builder = new Response.Builder();
//                    builder.request(chain.request());
//                    builder.protocol(Protocol.HTTP_1_0);
//                    builder.addHeader("content-type", mContentType);
//
//                    builder.body(ResponseBody.create(MediaType.parse(mContentType), responseStringBuilder.toString().getBytes()));
//                    builder.code(200);
//                    builder.message(responseStringBuilder.toString());
//
//                    response = builder.build();
//                }
//            }else {
//                response = chain.proceed(chain.request());
//            }


        } catch (UnknownHostException uhe) {
            Log.e(TAG, uhe.getMessage(), uhe);
            throw uhe;
        } catch (IOException ioe) {
            Log.e(TAG, ioe.getMessage(), ioe);
            throw ioe;
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
            throw e;
        }
        return response.newBuilder().body(body).build();
    }

    private String obfuscateConfig(String body) {
        if (type == RESPONSE_TYPE.JSON) {
            return obfuscate(body, "(?i)(\"\\s*password\\s*\")(\\s*:\\s*)(\\s*\")(.*?)(\")", 4, "xxx").replaceAll("\\r\\s*\\n", "\n");
        } else {
            return obfuscate(body, "(?i)(\\<\\s*password\\s*>)(.*?)(\\<\\/\\s*password\\s*>)", 2, "xxx").replaceAll("\\r\\s*\\n", "\n");
        }
    }

    public static String obfuscate(String text, String pattern, int groupToReplace, String replaceString) {
        Matcher m = Pattern.compile(pattern).matcher(text);
        StringBuffer sb = new StringBuffer();
        int lastIndex = 0;
        while (m.find()) {
            sb.append(text.substring(lastIndex, m.start(groupToReplace)));
            lastIndex = m.end(groupToReplace);
            sb.append(replaceString);
        }
        sb.append(text.substring(lastIndex));
        return sb.toString();
    }

    private String getFileName(Chain chain) {

        String method = chain.request().method();
        if (method.equals("GET")) {
            String fileName = null;
            String lastSegment = chain.request().url().pathSegments().get(chain.request().url().pathSegments().size() - 1);

            if (lastSegment.equals("filters")) {
                fileName = "filters";
            } else {
                fileName = lastSegment;
            }

            return fileName + FILE_EXTENSION;
        }
        return "none" + FILE_EXTENSION;
    }

    private String getFilePath(URI uri, String fileName) {
        String path;
        if (uri.getPath().lastIndexOf('/') != uri.getPath().length() - 1) {
            path = uri.getPath().substring(0, uri.getPath().lastIndexOf('/') + 1);
        } else {
            path = uri.getPath();
        }
        return uri.getHost() + path + fileName;
    }


}
