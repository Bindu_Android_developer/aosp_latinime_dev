package com.android.inputmethod.latin.http;

/**
 * Created by bkasmoor on 29/03/18.
 */

import com.android.inputmethod.latin.http.Response.FilterResponse;
import com.android.inputmethod.latin.http.Response.SearchResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RestAPIInterface {
    @Headers({"Content-Type: application/json"})
    @GET("Filter.php")
    public Call<FilterResponse> getFilters();

    @Headers({"Content-Type: application/json"})
    @GET("Search.php")
    public Call<SearchResponse> getSearchResults();
}
