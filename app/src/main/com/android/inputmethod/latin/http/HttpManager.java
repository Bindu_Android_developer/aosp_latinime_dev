package com.android.inputmethod.latin.http;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.android.inputmethod.latin.http.Response.FilterResponse;
import com.android.inputmethod.latin.http.Response.SearchResponse;
import com.android.inputmethod.listener.FiltersCallbackListener;
import com.android.inputmethod.listener.SearchCallBackListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Subscriber;

/**
 * Created by bkasmoor on 29/03/18.
 */

public class HttpManager {
    protected static final String TAG = "HttpManager";

    protected static HttpManager instance;

    protected static Context mContext;

    protected RestAPIInterface mHttpService;

    protected ExecutorService executor;

    protected GsonConverterFactory umsGsonConverter;

    private Retrofit mUMSRestAdapter;

    private List<HttpLoggingInterceptor> mIHSHttpLogging;

    private List<HttpLoggingInterceptor> mUMSHttpLogging;

    private String mServerEndPoint = "http://34.193.13.46/MOCK/";

    private FilterResponse getFilterResponse;

    private SearchResponse getSearchResultsResponse;

    private int filterErrorCode = -1;

    protected HttpManager() {
        Log.d(TAG, "HttpManager instance created");
    }

    public static void init(Context context) {
        mContext = context;
    }

    public static HttpManager getInstance() {
        synchronized (TAG) {
            if (instance == null) {
                instance = new HttpManager();
            }
            return instance;
        }
    }

    public static Context getContext() {
        return mContext;
    }

    void createUMSGsonConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setExclusionStrategies(new SerializeExclusionStrategy());

        Gson gson = gsonBuilder.create();
        umsGsonConverter = GsonConverterFactory.create(gson);
    }


    protected void checkAndCreateExecutor() {
        if (executor == null) {
            executor = Executors.newFixedThreadPool(4);
        }
    }

    private List<HttpLoggingInterceptor> getHttpLoggingInterceptor(OkHttpClient httpClient) {
        if (httpClient == null) {
            return null;
        }
        List<HttpLoggingInterceptor> loggingInterceptorList = new ArrayList<HttpLoggingInterceptor>();
        List<Interceptor> interceptors = httpClient.interceptors();
        if (interceptors != null) {
            for (Interceptor interceptor : interceptors) {
                if (interceptor instanceof HttpLoggingInterceptor) {
                    loggingInterceptorList.add((HttpLoggingInterceptor) interceptor);
                }
            }
        }
        return loggingInterceptorList;
    }


    protected OkHttpClient.Builder getOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(httpLoggingInterceptor);
        builder.connectTimeout(5, TimeUnit.MINUTES);
        builder.writeTimeout(5, TimeUnit.MINUTES);
        builder.readTimeout(5, TimeUnit.MINUTES);
        return builder;

    }



    private void createIHSHttpService() {
        if (umsGsonConverter == null) {
            createUMSGsonConverter();
        }
        if (mHttpService == null) {
            Log.d(TAG, "mHttpService is  null. Trying to recreate ");

                OkHttpClient.Builder builder = getOkHttpClient();
                builder.interceptors().add(new ConfigInterceptor(ConfigInterceptor.RESPONSE_TYPE.JSON, mContext));
                OkHttpClient httpClient = builder.build();
                mIHSHttpLogging = getHttpLoggingInterceptor(httpClient);
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(mServerEndPoint)
                        .client(httpClient)
                        .addConverterFactory(umsGsonConverter)
                        .build();
                mHttpService = retrofit.create(RestAPIInterface.class);
        }else{
            Log.d(TAG, "mHttpService is not null ");
        }
    }

    public void getFilters(final FiltersCallbackListener callbackListener){
        HttpManager.getInstance().getFilterResponse(new Subscriber<FilterResponse>() {
            @Override
            public void onCompleted() {
                Log.d(TAG, "onCompleted");
            }

            @Override
            public void onError(Throwable throwable) {
                Log.d(TAG, "onError:: "+throwable.getMessage());
            }

            @Override
            public void onNext(FilterResponse response) {
                if(response != null) {
                    if(response.getFilters() != null && response.getFilters().size() > 0)
                    callbackListener.onSuccess(response.getFilters());
                }else{
                    callbackListener.onFailure();
                }
            }
        }, callbackListener);
    }


    private void getFilterResponse(final Subscriber<FilterResponse> subscriber, final FiltersCallbackListener callbackListener){
        String serverURL = mServerEndPoint;
        if (!TextUtils.isEmpty(serverURL) && (!serverURL.endsWith("/"))) {
            serverURL += "/";
            mServerEndPoint = serverURL;
        }
        createIHSHttpService();
        Log.d(TAG, "Fetching get Filters state");
        if (mHttpService == null) {
            Log.d(TAG, "NOT Making to get Filters state since HTTP Service is null");
            return;
        }

        checkAndCreateExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                Callback<FilterResponse> callback = new Callback<FilterResponse>() {
                    @Override
                    public void onResponse(final Call<FilterResponse> call, final Response<FilterResponse> response) {
                        executor.execute(new Runnable() {
                            @Override
                            public void run() {
                                getFilterResponse = response.body();
                                filterErrorCode = response.code();
                                processFiltersResponse(subscriber, callbackListener);
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<FilterResponse> call, Throwable error) {
                        Log.e(TAG, "getFilterResponse Failed ", error);
                        subscriber.onError(error);
                    }
                };

                Call<FilterResponse> call = mHttpService.getFilters();
                call.enqueue(callback);
            }
        });
        return;

    }

    private void processFiltersResponse(Subscriber<FilterResponse> subscriber, FiltersCallbackListener callbackListener) {
        Log.d(TAG, "Received filters response");
        if (getFilterResponse != null) {
            if (filterErrorCode == 200) {
                String data = new Gson().toJson(getFilterResponse);
                Log.d(TAG, "filter response data is:: "+data);
                subscriber.onNext(getFilterResponse);
                subscriber.onCompleted();
            }
        }
    }


    public void getSearchResults(final SearchCallBackListener callbackListener){
        HttpManager.getInstance().getSearchResultResponse(new Subscriber<SearchResponse>() {
            @Override
            public void onCompleted() {
                Log.d(TAG, "onCompleted");
            }

            @Override
            public void onError(Throwable throwable) {
                Log.d(TAG, "onError:: "+throwable.getMessage());
            }

            @Override
            public void onNext(SearchResponse response) {
                if(response != null) {
                    if(response.getStickers() != null && response.getStickers().size() > 0)
                        callbackListener.onSuccess(response.getStickers());
                }else{
                    callbackListener.onFailure();
                }
            }
        }, callbackListener);
    }


    private void getSearchResultResponse(final Subscriber<SearchResponse> subscriber, final SearchCallBackListener callbackListener){
        String serverURL = mServerEndPoint;
        if (!TextUtils.isEmpty(serverURL) && (!serverURL.endsWith("/"))) {
            serverURL += "/";
            mServerEndPoint = serverURL;
        }
        createIHSHttpService();
        Log.d(TAG, "Fetching get Filters state");
        if (mHttpService == null) {
            Log.d(TAG, "NOT Making to get search results state since HTTP Service is null");
            return;
        }

        checkAndCreateExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                Callback<SearchResponse> callback = new Callback<SearchResponse>() {
                    @Override
                    public void onResponse(final Call<SearchResponse> call, final Response<SearchResponse> response) {
                        executor.execute(new Runnable() {
                            @Override
                            public void run() {
                                getSearchResultsResponse = response.body();
                                filterErrorCode = response.code();
                                processSearchResultsResponse(subscriber, callbackListener);
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<SearchResponse> call, Throwable error) {
                        Log.e(TAG, "getSearchResultsResponse Failed ", error);
                        subscriber.onError(error);
                    }
                };

                Call<SearchResponse> call = mHttpService.getSearchResults();
                call.enqueue(callback);
            }
        });
        return;

    }

    private void processSearchResultsResponse(Subscriber<SearchResponse> subscriber, SearchCallBackListener callbackListener) {
        Log.d(TAG, "Received search Results response");
        if (getSearchResultsResponse != null) {
            if (filterErrorCode == 200) {
                String data = new Gson().toJson(getSearchResultsResponse);
                Log.d(TAG, "search results response data is:: "+data);
                subscriber.onNext(getSearchResultsResponse);
                subscriber.onCompleted();
            }
        }
    }


}
