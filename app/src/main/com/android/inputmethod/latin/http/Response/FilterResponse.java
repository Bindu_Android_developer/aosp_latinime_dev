package com.android.inputmethod.latin.http.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bkasmoor on 30/03/18.
 */

public class FilterResponse {
    @SerializedName("Filters")
    @Expose
    private ArrayList<String> filters = null;

    public ArrayList<String> getFilters() {
        return filters;
    }

    public void setFilters(ArrayList<String> filters) {
        this.filters = filters;
    }


}
