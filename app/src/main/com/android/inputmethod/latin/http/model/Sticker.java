package com.android.inputmethod.latin.http.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by bkasmoor on 29/03/18.
 */

public class Sticker {
    @SerializedName("Category")
    @Expose
    private String category;
    @SerializedName("Imagepath")
    @Expose
    private String imagepath;
    @SerializedName("Subcategory")
    @Expose
    private String subcategory;
    @SerializedName("ImageExtension")
    @Expose
    private String imageExtension;
    @SerializedName("ImageID")
    @Expose
    private String imageID;
    @SerializedName("Tokens")
    @Expose
    private String tokens;
    @SerializedName("ImageSizeFormat")
    @Expose
    private String imageSizeFormat;
    @SerializedName("Keywords")
    @Expose
    private String keywords;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getImageExtension() {
        return imageExtension;
    }

    public void setImageExtension(String imageExtension) {
        this.imageExtension = imageExtension;
    }

    public String getImageID() {
        return imageID;
    }

    public void setImageID(String imageID) {
        this.imageID = imageID;
    }

    public String getTokens() {
        return tokens;
    }

    public void setTokens(String tokens) {
        this.tokens = tokens;
    }

    public String getImageSizeFormat() {
        return imageSizeFormat;
    }

    public void setImageSizeFormat(String imageSizeFormat) {
        this.imageSizeFormat = imageSizeFormat;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

}
