package com.android.inputmethod.latin.http.Response;

import com.android.inputmethod.latin.http.model.Sticker;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by bkasmoor on 05/04/18.
 */

public class SearchResponse {
    @SerializedName("TimeInterval")
    @Expose
    private String timeInterval;
    @SerializedName("Stickers")
    @Expose
    private ArrayList<Sticker> stickers = null;
    @SerializedName("UserID")
    @Expose
    private String userID;
    @SerializedName("TaskID")
    @Expose
    private Object taskID;

    public String getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(String timeInterval) {
        this.timeInterval = timeInterval;
    }

    public ArrayList<Sticker> getStickers() {
        return stickers;
    }

    public void setStickers(ArrayList<Sticker> stickers) {
        this.stickers = stickers;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public Object getTaskID() {
        return taskID;
    }

    public void setTaskID(Object taskID) {
        this.taskID = taskID;
    }
}
