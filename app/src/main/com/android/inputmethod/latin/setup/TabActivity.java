package com.android.inputmethod.latin.setup;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.android.inputmethod.R;
import com.android.inputmethod.adapter.OrderedTabs;
import com.android.inputmethod.adapter.TabInterface;
import com.android.inputmethod.adapter.TabsAdapter;
import com.android.inputmethod.fragments.FilterFragment;
import com.android.inputmethod.fragments.GlobeFragment;
import com.android.inputmethod.fragments.KeypadFragment;
import com.android.inputmethod.fragments.SearchFragment;
import com.android.inputmethod.latin.http.HttpManager;

import java.util.ArrayList;

/**
 * Created by bkasmoor on 30/03/18.
 */

public class TabActivity extends AppCompatActivity {
    private static TabsAdapter tabsAdapter;

    private ViewPager viewPager;

    private TabLayout tabLayout;

    private String defaultTagName;

    private String firstTagName;

    private boolean areTabsInitialized;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_tabs_frame );
        HttpManager.init(getApplicationContext());
        initialize();

    }

    private void initialize() {
//        WindowManager.LayoutParams params = getWindow().getAttributes();
//        params.x = -100;
//        params.height = 70;
//        params.width = 1000;
//        params.y = -50;
//
//        this.getWindow().setAttributes(params);

        tabLayout = (TabLayout)findViewById(R.id.tab_layout);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = (ViewPager)findViewById(R.id.pager);
        viewPager.setBackgroundResource(android.R.color.white);
        viewPager.setOffscreenPageLimit(4);

        initTabs();
    }

    private void initTabs() {
        areTabsInitialized = false;

        tabLayout.removeAllTabs();
        tabsAdapter = new TabsAdapter((FragmentActivity)this, tabLayout, viewPager);


        defaultTagName = null;
        firstTagName = null;
        ArrayList<OrderedTabs> orderedTabsList = getDefaultOrderedTabsList();
        if (orderedTabsList != null) {
            //tab order has been overwritten
            for (OrderedTabs orderedTab : orderedTabsList) {
                if (OrderedTabs.GLOBE_TAB_NAME.equals(orderedTab.getName())) {
                    handleGlobeTab();
                } else if (OrderedTabs.SEARCH_TAB_NAME.equals(orderedTab.getName())) {
                    handleSearchTab();
                } else if (OrderedTabs.FILTER_TAB_NAME.equals(orderedTab.getName())) {
                    handleFilterTab();
                } else if (OrderedTabs.KEYPAD_TAB_NAME.equals(orderedTab.getName())) {
                    handleKeypadTab();
                }
            }
        }
        areTabsInitialized = true;
        if (TextUtils.isEmpty(defaultTagName)) {
            defaultTagName = firstTagName;
        }

        tabsAdapter.setInitialTab(defaultTagName);
        tabsAdapter.setListeners();

    }


    public ArrayList<OrderedTabs> getDefaultOrderedTabsList() {
        ArrayList<OrderedTabs> orderedTabs = new ArrayList<OrderedTabs>();
        orderedTabs.add(new OrderedTabs(OrderedTabs.GLOBE_TAB_NAME));
        orderedTabs.add(new OrderedTabs(OrderedTabs.SEARCH_TAB_NAME));
        orderedTabs.add(new OrderedTabs(OrderedTabs.FILTER_TAB_NAME));
        orderedTabs.add(new OrderedTabs(OrderedTabs.KEYPAD_TAB_NAME));
        return orderedTabs;
    }

    private void handleGlobeTab() {
        createTab(OrderedTabs.GLOBE_TAB_NAME, R.string.tabname_globe, R.id.id_tab_globe, ContextCompat.getDrawable(this, R.drawable.tab_contacts), GlobeFragment.class, R.string.tabname_globe);
        if (firstTagName == null) {
            firstTagName = OrderedTabs.GLOBE_TAB_NAME;
        }
    }

    private void handleSearchTab() {
        createTab(OrderedTabs.SEARCH_TAB_NAME, R.string.tabname_search, R.id.id_tab_search, ContextCompat.getDrawable(this, R.drawable.tab_calllog), SearchFragment.class, R.string.tabname_search);
        if (firstTagName == null) {
            firstTagName = OrderedTabs.SEARCH_TAB_NAME;
        }


    }

    private void handleFilterTab() {
        createTab(OrderedTabs.FILTER_TAB_NAME, R.string.tabname_filter, R.id.id_tab_filter, ContextCompat.getDrawable(this, R.drawable.tab_voicemail), FilterFragment.class, R.string.tabname_filter);
        if (firstTagName == null) {
            firstTagName = OrderedTabs.FILTER_TAB_NAME;
        }
    }

    private void handleKeypadTab() {
        createTab(OrderedTabs.KEYPAD_TAB_NAME, R.string.tabname_keypad, R.id.id_tab_keypad, ContextCompat.getDrawable(this, R.drawable.tab_app_settings), KeypadFragment.class, R.string.tabname_keypad);
        if (firstTagName == null) {
            firstTagName = OrderedTabs.KEYPAD_TAB_NAME;
        }

    }

    private void createTab(String tag, final int tabNameResId, final int tabId, Drawable drawable, Class<?> fragment, final int tabHeaderNameRes) {

        tabsAdapter.addTab(tag, drawable, fragment);

        LayoutInflater inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
        RelativeLayout tabContent = (RelativeLayout) inflater.inflate(R.layout.keyboard_tabcontent, null);
        tabContent.setId(tabId);
        ImageView icon = (ImageView) tabContent.findViewById(R.id.tab_icon);
        icon.setImageDrawable(drawable);
        TextView tabText = (TextView) tabContent.findViewById(R.id.tab_text);
        tabText.setText(getString(tabHeaderNameRes));

        tabContent.setLayoutParams(new TabLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TabLayout.LayoutParams.MATCH_PARENT));

        tabLayout.setOnTabSelectedListener(new TabSwitchListener<Fragment>()); //TODO - move as generic listener - don't recreate for each tab
        final TabLayout.Tab tab = tabLayout
                .newTab()
                .setTag(tag)
                .setCustomView(tabContent);
        tab.getCustomView().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!tab.isSelected()) {
                    tab.select();
                }
            }
        });

        tabLayout.addTab(tab);

    }

    public final class TabSwitchListener<T extends Fragment> implements TabLayout.OnTabSelectedListener {
        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabSelected(TabLayout.Tab tab) {

            TextView curTabTextView = tab.getCustomView().findViewById(R.id.tab_text);
            tab.getCustomView().findViewById(R.id.tab_icon).setPadding(0,6,0,0);
            curTabTextView.setTextSize(14);

            tabsAdapter.setCurrentItem(tab.getPosition());


            TabInterface adapter = (TabInterface) tabsAdapter.getItem(tab.getPosition());
            if (adapter != null) {
                adapter.onTabSelected();
                return;
            }
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

            TextView curTabTextView = tab.getCustomView().findViewById(R.id.tab_text);
            tab.getCustomView().findViewById(R.id.tab_icon).setPadding(0,8,0,0);
            curTabTextView.setTextSize(12);

            TabInterface adapter = (TabInterface) tabsAdapter.getItem(tab.getPosition());
            if (adapter != null) {
                adapter.onTabUnselected();
                return;
            }
        }
    }



}
