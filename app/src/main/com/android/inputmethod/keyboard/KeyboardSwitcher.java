/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.inputmethod.keyboard;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inputmethod.R;
import com.android.inputmethod.Utils.AppUtils;
import com.android.inputmethod.adapter.FilterAdapter;
import com.android.inputmethod.adapter.SearchResultsAdapter;
import com.android.inputmethod.compat.InputMethodServiceCompatUtils;
import com.android.inputmethod.event.Event;
import com.android.inputmethod.fragments.FilterFragment;
import com.android.inputmethod.keyboard.KeyboardLayoutSet.KeyboardLayoutSetException;
import com.android.inputmethod.keyboard.emoji.EmojiPalettesView;
import com.android.inputmethod.keyboard.internal.CustomPagerAdapter;
import com.android.inputmethod.keyboard.internal.KeyboardState;
import com.android.inputmethod.keyboard.internal.KeyboardTextsSet;
import com.android.inputmethod.latin.InputView;
import com.android.inputmethod.latin.LatinIME;
import com.android.inputmethod.latin.RichInputMethodManager;
import com.android.inputmethod.latin.WordComposer;
import com.android.inputmethod.latin.common.Constants;
import com.android.inputmethod.latin.define.ProductionFlags;
import com.android.inputmethod.latin.http.HttpManager;
import com.android.inputmethod.latin.http.model.Sticker;
import com.android.inputmethod.latin.settings.Settings;
import com.android.inputmethod.latin.settings.SettingsValues;
import com.android.inputmethod.latin.utils.CapsModeUtils;
import com.android.inputmethod.latin.utils.LanguageOnSpacebarUtils;
import com.android.inputmethod.latin.utils.RecapitalizeStatus;
import com.android.inputmethod.latin.utils.ResourceUtils;
import com.android.inputmethod.latin.utils.ScriptUtils;
import com.android.inputmethod.listener.FiltersCallbackListener;
import com.android.inputmethod.listener.SearchCallBackListener;
import com.android.inputmethod.listener.onKeyEnter;

import java.util.ArrayList;
import java.util.logging.Handler;

import javax.annotation.Nonnull;

public final class KeyboardSwitcher implements KeyboardState.SwitchActions {
    private static final String TAG = KeyboardSwitcher.class.getSimpleName();

    private InputView mCurrentInputView;
    private View mMainKeyboardFrame;
    private MainKeyboardView mKeyboardView;
    private EmojiPalettesView mEmojiPalettesView;
    private LatinIME mLatinIME;
    private RichInputMethodManager mRichImm;
    private boolean mIsHardwareAcceleratedDrawingEnabled;
   // private SearchView mSearchView;
    private RelativeLayout mSearchView;
    private EditText editText;

    private ImageView backButton, searchCloseButton;

    private KeyboardState mState;

    private KeyboardLayoutSet mKeyboardLayoutSet;
    // TODO: The following {@link KeyboardTextsSet} should be in {@link KeyboardLayoutSet}.
    private final KeyboardTextsSet mKeyboardTextsSet = new KeyboardTextsSet();

    private KeyboardTheme mKeyboardTheme;
    private Context mThemeContext;

    private LinearLayout tabView,tabViewParent;
    private RelativeLayout globe,search,filter,latin;
    private RelativeLayout globe_parent_layout,latin_parent_layout,search_parent_layout,filter_parent_layout;
    private static final KeyboardSwitcher sInstance = new KeyboardSwitcher();
    private static android.os.Handler mHandler;
    private ViewPager viewPager;
    private CustomPagerAdapter adapter;

    private RecyclerView filter_recyclerView, search_recyclerview;
    private ArrayList<String> data;
    private ArrayList<Sticker> stickersData;
    private FilterAdapter filterAdapter;
    private SearchResultsAdapter searchResultsAdapter;
    private RelativeLayout progress_layout;
    private android.os.Handler uiHandler;

    public static KeyboardSwitcher getInstance(android.os.Handler handler) {
        mHandler = handler;
        return sInstance;
    }

    private KeyboardSwitcher() {
        // Intentional empty constructor for singleton.
    }

    public static void init(final LatinIME latinIme) {
        sInstance.initInternal(latinIme);
    }

    private void initInternal(final LatinIME latinIme) {
        mLatinIME = latinIme;
        mRichImm = RichInputMethodManager.getInstance();
        mState = new KeyboardState(this);
        mIsHardwareAcceleratedDrawingEnabled =
                InputMethodServiceCompatUtils.enableHardwareAcceleration(mLatinIME);
    }

    public void updateKeyboardTheme() {
        final boolean themeUpdated = updateKeyboardThemeAndContextThemeWrapper(
                mLatinIME, KeyboardTheme.getKeyboardTheme(mLatinIME /* context */));
        if (themeUpdated && mKeyboardView != null) {
            mLatinIME.setInputView(onCreateInputView(mIsHardwareAcceleratedDrawingEnabled));
        }
    }

    private boolean updateKeyboardThemeAndContextThemeWrapper(final Context context,
            final KeyboardTheme keyboardTheme) {
        if (mThemeContext == null || !keyboardTheme.equals(mKeyboardTheme)) {
            mKeyboardTheme = keyboardTheme;
            mThemeContext = new ContextThemeWrapper(context, keyboardTheme.mStyleId);
            KeyboardLayoutSet.onKeyboardThemeChanged();
            return true;
        }
        return false;
    }

    public void loadKeyboard(final EditorInfo editorInfo, final SettingsValues settingsValues,
            final int currentAutoCapsState, final int currentRecapitalizeState) {
        final KeyboardLayoutSet.Builder builder = new KeyboardLayoutSet.Builder(
                mThemeContext, editorInfo);
        final Resources res = mThemeContext.getResources();
        final int keyboardWidth = ResourceUtils.getDefaultKeyboardWidth(res);
        final int keyboardHeight = ResourceUtils.getKeyboardHeight(res, settingsValues);
        builder.setKeyboardGeometry(keyboardWidth, keyboardHeight);
        builder.setSubtype(mRichImm.getCurrentSubtype());
        builder.setVoiceInputKeyEnabled(settingsValues.mShowsVoiceInputKey);
        builder.setLanguageSwitchKeyEnabled(mLatinIME.shouldShowLanguageSwitchKey());
        builder.setSplitLayoutEnabledByUser(ProductionFlags.IS_SPLIT_KEYBOARD_SUPPORTED
                && settingsValues.mIsSplitKeyboardEnabled);
        mKeyboardLayoutSet = builder.build();
        try {
            mState.onLoadKeyboard(currentAutoCapsState, currentRecapitalizeState);
            mKeyboardTextsSet.setLocale(mRichImm.getCurrentSubtypeLocale(), mThemeContext);
        } catch (KeyboardLayoutSetException e) {
            Log.w(TAG, "loading keyboard failed: " + e.mKeyboardId, e.getCause());
        }
    }

    public void saveKeyboardState() {
        if (getKeyboard() != null || isShowingEmojiPalettes()) {
            mState.onSaveKeyboardState();
        }
    }

    public void onHideWindow() {
        if (mKeyboardView != null) {
            mKeyboardView.onHideWindow();
        }
    }

    private void setKeyboard(
            @Nonnull final int keyboardId,
            @Nonnull final KeyboardSwitchState toggleState) {
        // Make {@link MainKeyboardView} visible and hide {@link EmojiPalettesView}.
        final SettingsValues currentSettingsValues = Settings.getInstance().getCurrent();
        setMainKeyboardFrame(currentSettingsValues, toggleState);
        // TODO: pass this object to setKeyboard instead of getting the current values.
        final MainKeyboardView keyboardView = mKeyboardView;
        final Keyboard oldKeyboard = keyboardView.getKeyboard();
        final Keyboard newKeyboard = mKeyboardLayoutSet.getKeyboard(keyboardId);
        keyboardView.setKeyboard(newKeyboard);
        mCurrentInputView.setKeyboardTopPadding(newKeyboard.mTopPadding);
        keyboardView.setKeyPreviewPopupEnabled(
                currentSettingsValues.mKeyPreviewPopupOn,
                currentSettingsValues.mKeyPreviewPopupDismissDelay);
        keyboardView.setKeyPreviewAnimationParams(
                currentSettingsValues.mHasCustomKeyPreviewAnimationParams,
                currentSettingsValues.mKeyPreviewShowUpStartXScale,
                currentSettingsValues.mKeyPreviewShowUpStartYScale,
                currentSettingsValues.mKeyPreviewShowUpDuration,
                currentSettingsValues.mKeyPreviewDismissEndXScale,
                currentSettingsValues.mKeyPreviewDismissEndYScale,
                currentSettingsValues.mKeyPreviewDismissDuration);
        keyboardView.updateShortcutKey(mRichImm.isShortcutImeReady());
        final boolean subtypeChanged = (oldKeyboard == null)
                || !newKeyboard.mId.mSubtype.equals(oldKeyboard.mId.mSubtype);
        final int languageOnSpacebarFormatType = LanguageOnSpacebarUtils
                .getLanguageOnSpacebarFormatType(newKeyboard.mId.mSubtype);
        final boolean hasMultipleEnabledIMEsOrSubtypes = mRichImm
                .hasMultipleEnabledIMEsOrSubtypes(true /* shouldIncludeAuxiliarySubtypes */);
        keyboardView.startDisplayLanguageOnSpacebar(subtypeChanged, languageOnSpacebarFormatType,
                hasMultipleEnabledIMEsOrSubtypes);
    }

    public Keyboard getKeyboard() {
        if (mKeyboardView != null) {
            return mKeyboardView.getKeyboard();
        }
        return null;
    }

    // TODO: Remove this method. Come up with a more comprehensive way to reset the keyboard layout
    // when a keyboard layout set doesn't get reloaded in LatinIME.onStartInputViewInternal().
    public void resetKeyboardStateToAlphabet(final int currentAutoCapsState,
            final int currentRecapitalizeState) {
        mState.onResetKeyboardStateToAlphabet(currentAutoCapsState, currentRecapitalizeState);
    }

    public void onPressKey(final int code, final boolean isSinglePointer,
            final int currentAutoCapsState, final int currentRecapitalizeState) {
        mState.onPressKey(code, isSinglePointer, currentAutoCapsState, currentRecapitalizeState);
    }

    public void onReleaseKey(final int code, final boolean withSliding,
            final int currentAutoCapsState, final int currentRecapitalizeState) {
        mState.onReleaseKey(code, withSliding, currentAutoCapsState, currentRecapitalizeState);
    }

    public void onFinishSlidingInput(final int currentAutoCapsState,
            final int currentRecapitalizeState) {
        mState.onFinishSlidingInput(currentAutoCapsState, currentRecapitalizeState);
    }

    // Implements {@link KeyboardState.SwitchActions}.
    @Override
    public void setAlphabetKeyboard() {
        if (DEBUG_ACTION) {
            Log.d(TAG, "setAlphabetKeyboard");
        }
        setKeyboard(KeyboardId.ELEMENT_ALPHABET, KeyboardSwitchState.OTHER);
    }

    // Implements {@link KeyboardState.SwitchActions}.
    @Override
    public void setAlphabetManualShiftedKeyboard() {
        if (DEBUG_ACTION) {
            Log.d(TAG, "setAlphabetManualShiftedKeyboard");
        }
        setKeyboard(KeyboardId.ELEMENT_ALPHABET_MANUAL_SHIFTED, KeyboardSwitchState.OTHER);
    }

    // Implements {@link KeyboardState.SwitchActions}.
    @Override
    public void setAlphabetAutomaticShiftedKeyboard() {
        if (DEBUG_ACTION) {
            Log.d(TAG, "setAlphabetAutomaticShiftedKeyboard");
        }
        setKeyboard(KeyboardId.ELEMENT_ALPHABET_AUTOMATIC_SHIFTED, KeyboardSwitchState.OTHER);
    }

    // Implements {@link KeyboardState.SwitchActions}.
    @Override
    public void setAlphabetShiftLockedKeyboard() {
        if (DEBUG_ACTION) {
            Log.d(TAG, "setAlphabetShiftLockedKeyboard");
        }
        setKeyboard(KeyboardId.ELEMENT_ALPHABET_SHIFT_LOCKED, KeyboardSwitchState.OTHER);
    }

    // Implements {@link KeyboardState.SwitchActions}.
    @Override
    public void setAlphabetShiftLockShiftedKeyboard() {
        if (DEBUG_ACTION) {
            Log.d(TAG, "setAlphabetShiftLockShiftedKeyboard");
        }
        setKeyboard(KeyboardId.ELEMENT_ALPHABET_SHIFT_LOCK_SHIFTED, KeyboardSwitchState.OTHER);
    }

    // Implements {@link KeyboardState.SwitchActions}.
    @Override
    public void setSymbolsKeyboard() {
        if (DEBUG_ACTION) {
            Log.d(TAG, "setSymbolsKeyboard");
        }
        setKeyboard(KeyboardId.ELEMENT_SYMBOLS, KeyboardSwitchState.OTHER);
    }

    // Implements {@link KeyboardState.SwitchActions}.
    @Override
    public void setSymbolsShiftedKeyboard() {
        if (DEBUG_ACTION) {
            Log.d(TAG, "setSymbolsShiftedKeyboard");
        }
        setKeyboard(KeyboardId.ELEMENT_SYMBOLS_SHIFTED, KeyboardSwitchState.SYMBOLS_SHIFTED);
    }

    public boolean isImeSuppressedByHardwareKeyboard(
            @Nonnull final SettingsValues settingsValues,
            @Nonnull final KeyboardSwitchState toggleState) {
        return settingsValues.mHasHardwareKeyboard && toggleState == KeyboardSwitchState.HIDDEN;
    }

    private void setMainKeyboardFrame(
            @Nonnull final SettingsValues settingsValues,
            @Nonnull final KeyboardSwitchState toggleState) {
        final int visibility =  isImeSuppressedByHardwareKeyboard(settingsValues, toggleState)
                ? View.GONE : View.VISIBLE;
        mKeyboardView.setVisibility(visibility);
        // The visibility of {@link #mKeyboardView} must be aligned with {@link #MainKeyboardFrame}.
        // @see #getVisibleKeyboardView() and
        // @see LatinIME#onComputeInset(android.inputmethodservice.InputMethodService.Insets)
        mMainKeyboardFrame.setVisibility(visibility);
        mEmojiPalettesView.setVisibility(View.GONE);
        mEmojiPalettesView.stopEmojiPalettes();
    }

    // Implements {@link KeyboardState.SwitchActions}.
    @Override
    public void setEmojiKeyboard() {
        if (DEBUG_ACTION) {
            Log.d(TAG, "setEmojiKeyboard");
        }
        final Keyboard keyboard = mKeyboardLayoutSet.getKeyboard(KeyboardId.ELEMENT_ALPHABET);
        mMainKeyboardFrame.setVisibility(View.GONE);
        // The visibility of {@link #mKeyboardView} must be aligned with {@link #MainKeyboardFrame}.
        // @see #getVisibleKeyboardView() and
        // @see LatinIME#onComputeInset(android.inputmethodservice.InputMethodService.Insets)
        mKeyboardView.setVisibility(View.GONE);
        mEmojiPalettesView.startEmojiPalettes(
                mKeyboardTextsSet.getText(KeyboardTextsSet.SWITCH_TO_ALPHA_KEY_LABEL),
                mKeyboardView.getKeyVisualAttribute(), keyboard.mIconsSet);
        mEmojiPalettesView.setVisibility(View.VISIBLE);
    }

    public enum KeyboardSwitchState {
        HIDDEN(-1),
        SYMBOLS_SHIFTED(KeyboardId.ELEMENT_SYMBOLS_SHIFTED),
        EMOJI(KeyboardId.ELEMENT_EMOJI_RECENTS),
        OTHER(-1);

        final int mKeyboardId;

        KeyboardSwitchState(int keyboardId) {
            mKeyboardId = keyboardId;
        }
    }

    public void hideDefaultKeyboards(){
        if(mKeyboardView != null) {
            mKeyboardView.setVisibility(View.GONE);
        }
        if(mMainKeyboardFrame != null) {
            mMainKeyboardFrame.setVisibility(View.GONE);
        }
        if(mEmojiPalettesView != null) {
            mEmojiPalettesView.setVisibility(View.GONE);
            mEmojiPalettesView.stopEmojiPalettes();
        }
    }

    public KeyboardSwitchState getKeyboardSwitchState() {
        boolean hidden = !isShowingEmojiPalettes()
                && (mKeyboardLayoutSet == null
                || mKeyboardView == null
                || !mKeyboardView.isShown());
        KeyboardSwitchState state;
        if (hidden) {
            return KeyboardSwitchState.HIDDEN;
        } else if (isShowingEmojiPalettes()) {
            return KeyboardSwitchState.EMOJI;
        } else if (isShowingKeyboardId(KeyboardId.ELEMENT_SYMBOLS_SHIFTED)) {
            return KeyboardSwitchState.SYMBOLS_SHIFTED;
        }
        return KeyboardSwitchState.OTHER;
    }

    public void onToggleKeyboard(@Nonnull final KeyboardSwitchState toggleState) {
        KeyboardSwitchState currentState = getKeyboardSwitchState();
        Log.w(TAG, "onToggleKeyboard() : Current = " + currentState + " : Toggle = " + toggleState);
        if (currentState == toggleState) {
            mLatinIME.stopShowingInputView();
            mLatinIME.hideWindow();
            setAlphabetKeyboard();
        } else {
            mLatinIME.startShowingInputView(true);
            if (toggleState == KeyboardSwitchState.EMOJI) {
                setEmojiKeyboard();
            } else {
                mEmojiPalettesView.stopEmojiPalettes();
                mEmojiPalettesView.setVisibility(View.GONE);

                mMainKeyboardFrame.setVisibility(View.VISIBLE);
                mKeyboardView.setVisibility(View.VISIBLE);
                setKeyboard(toggleState.mKeyboardId, toggleState);
            }
        }
    }

    // Future method for requesting an updating to the shift state.
    @Override
    public void requestUpdatingShiftState(final int autoCapsFlags, final int recapitalizeMode) {
        if (DEBUG_ACTION) {
            Log.d(TAG, "requestUpdatingShiftState: "
                    + " autoCapsFlags=" + CapsModeUtils.flagsToString(autoCapsFlags)
                    + " recapitalizeMode=" + RecapitalizeStatus.modeToString(recapitalizeMode));
        }
        mState.onUpdateShiftState(autoCapsFlags, recapitalizeMode);
    }

    // Implements {@link KeyboardState.SwitchActions}.
    @Override
    public void startDoubleTapShiftKeyTimer() {
        if (DEBUG_TIMER_ACTION) {
            Log.d(TAG, "startDoubleTapShiftKeyTimer");
        }
        final MainKeyboardView keyboardView = getMainKeyboardView();
        if (keyboardView != null) {
            keyboardView.startDoubleTapShiftKeyTimer();
        }
    }

    // Implements {@link KeyboardState.SwitchActions}.
    @Override
    public void cancelDoubleTapShiftKeyTimer() {
        if (DEBUG_TIMER_ACTION) {
            Log.d(TAG, "setAlphabetKeyboard");
        }
        final MainKeyboardView keyboardView = getMainKeyboardView();
        if (keyboardView != null) {
            keyboardView.cancelDoubleTapShiftKeyTimer();
        }
    }

    // Implements {@link KeyboardState.SwitchActions}.
    @Override
    public boolean isInDoubleTapShiftKeyTimeout() {
        if (DEBUG_TIMER_ACTION) {
            Log.d(TAG, "isInDoubleTapShiftKeyTimeout");
        }
        final MainKeyboardView keyboardView = getMainKeyboardView();
        return keyboardView != null && keyboardView.isInDoubleTapShiftKeyTimeout();
    }

    /**
     * Updates state machine to figure out when to automatically switch back to the previous mode.
     */
    public void onEvent(final Event event, final int currentAutoCapsState,
            final int currentRecapitalizeState) {
        mState.onEvent(event, currentAutoCapsState, currentRecapitalizeState);
    }

    public boolean isShowingKeyboardId(@Nonnull int... keyboardIds) {
        if (mKeyboardView == null || !mKeyboardView.isShown()) {
            return false;
        }
        int activeKeyboardId = mKeyboardView.getKeyboard().mId.mElementId;
        for (int keyboardId : keyboardIds) {
            if (activeKeyboardId == keyboardId) {
                return true;
            }
        }
        return false;
    }

    public boolean isShowingEmojiPalettes() {
        return mEmojiPalettesView != null && mEmojiPalettesView.isShown();
    }

    public boolean isShowingMoreKeysPanel() {
        if (isShowingEmojiPalettes()) {
            return false;
        }
        return mKeyboardView.isShowingMoreKeysPanel();
    }

    public View getVisibleKeyboardView() {
        if (isShowingEmojiPalettes()) {
            return mEmojiPalettesView;
        }
        return mKeyboardView;
    }

    public LinearLayout getTabView(){
        return tabView;//Parent;
    }
    public InputView getFullView(){
        return mCurrentInputView;//Parent;
    }

    public ViewPager getViewPager(){
        return viewPager;//Parent;
    }


    public void setDefaultHeight(){
        globe_parent_layout.setMinimumHeight(mMainKeyboardFrame.getHeight());
        search_parent_layout.setMinimumHeight(mMainKeyboardFrame.getHeight());
        filter_parent_layout.setMinimumHeight(mMainKeyboardFrame.getHeight());
    }

    public MainKeyboardView getMainKeyboardView() {
        return mKeyboardView;
    }

    public void deallocateMemory() {
        if (mKeyboardView != null) {
            mKeyboardView.cancelAllOngoingEvents();
            mKeyboardView.deallocateMemory();
        }
        if (mEmojiPalettesView != null) {
            mEmojiPalettesView.stopEmojiPalettes();
        }
    }

    public Context getContext(){
        return mThemeContext;
    }

    public View onCreateInputView(final boolean isHardwareAcceleratedDrawingEnabled) {
        if (mKeyboardView != null) {
            mKeyboardView.closing();
        }

        updateKeyboardThemeAndContextThemeWrapper(
                mLatinIME, KeyboardTheme.getKeyboardTheme(mLatinIME /* context */));
        mCurrentInputView = (InputView)LayoutInflater.from(mThemeContext).inflate(
        R.layout.input_view, null);

//        tabViewParent = (LinearLayout) LayoutInflater.from(mThemeContext).inflate(
//                R.layout.tab_test_view, null);//input_view //tab_view
//        mCurrentInputView = (InputView)tabView.findViewById(R.id.inputview);
        tabView = mCurrentInputView.findViewById(R.id.tabview);
        globe = mCurrentInputView.findViewById(R.id.globe);
        search = mCurrentInputView.findViewById(R.id.search);
        filter = mCurrentInputView.findViewById(R.id.filter);
        latin = mCurrentInputView.findViewById(R.id.latin);
        globe_parent_layout = mCurrentInputView.findViewById(R.id.globe_parent_layout);
        latin_parent_layout = mCurrentInputView.findViewById(R.id.latin_parent_layout);
        filter_parent_layout = mCurrentInputView.findViewById(R.id.filter_parent_layout);
        filter_recyclerView = (RecyclerView)mCurrentInputView.findViewById(R.id.filter_recycler_view);
        search_recyclerview = (RecyclerView)mCurrentInputView.findViewById(R.id.search_recycler_view) ;
        progress_layout = (RelativeLayout) mCurrentInputView.findViewById(R.id.progress_layout);
        progress_layout.setVisibility(View.GONE);
        progress_layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        uiHandler = new android.os.Handler();
        if(mThemeContext != null) {
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mThemeContext, 3);
            filter_recyclerView.setLayoutManager(mLayoutManager);
            filter_recyclerView.addItemDecoration(new GridSpacingItemDecoration(3, dpToPx(10), true));
            filter_recyclerView.setItemAnimator(new DefaultItemAnimator());

            RecyclerView.LayoutManager mSearchLayoutManager = new GridLayoutManager(mThemeContext, 3);
            search_recyclerview.setLayoutManager(mSearchLayoutManager);
            search_recyclerview.addItemDecoration(new GridSpacingItemDecoration(3, dpToPx(10), true));
            search_recyclerview.setItemAnimator(new DefaultItemAnimator());
        }
        search_parent_layout = mCurrentInputView.findViewById(R.id.search_parent_layout);
        mMainKeyboardFrame = mCurrentInputView.findViewById(R.id.main_keyboard_frame); //main_keyboard_frame //testView
        mEmojiPalettesView = (EmojiPalettesView)mCurrentInputView.findViewById(
                R.id.emoji_palettes_view);
//////
        mKeyboardView = (MainKeyboardView) mCurrentInputView.findViewById(R.id.keyboard_view);
        mKeyboardView.setHardwareAcceleratedDrawingEnabled(isHardwareAcceleratedDrawingEnabled);
        mKeyboardView.setKeyboardActionListener(mLatinIME);
        mEmojiPalettesView.setHardwareAcceleratedDrawingEnabled(
                isHardwareAcceleratedDrawingEnabled);
        mEmojiPalettesView.setKeyboardActionListener(mLatinIME);
//        mSearchView = (SearchView) mCurrentInputView.findViewById(R.id.searchView);
        mSearchView = (RelativeLayout) mCurrentInputView.findViewById(R.id.searchView);
        backButton = (ImageView) mCurrentInputView.findViewById(R.id.search_back);
        searchCloseButton = (ImageView) mCurrentInputView.findViewById(R.id.search_close_btn);
        editText = (EditText) mCurrentInputView.findViewById(R.id.edittext);//editText.edit


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                globe_parent_layout.setVisibility(View.GONE);
                search_parent_layout.setVisibility(View.GONE);
                filter_parent_layout.setVisibility(View.GONE);
                latin_parent_layout.setVisibility(View.VISIBLE);
                mSearchView.setVisibility(View.GONE);
                tabView.setVisibility(View.VISIBLE);
                editText.clearFocus();
                editText.setText("");
            }
        });
        searchCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText("");
            }
        });
//        tabView.setOnTouchListener(new View.OnTouchListener() {
//
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                Log.d("viecheck","tab click");
//                return true;
//            }
//        });
   //
        setTabListeners();
       // setPager(isHardwareAcceleratedDrawingEnabled);
        //return tabViewParent;
        mKeyboardView.setEditText(mSearchView, editText, new onKeyEnter() {

            @Override
            public void onPerformSearch() {
                Log.d("searchTest","onPerformSearch called");
                globe_parent_layout.setVisibility(View.GONE);
                search_parent_layout.setVisibility(View.VISIBLE);
                filter_parent_layout.setVisibility(View.GONE);
                latin_parent_layout.setVisibility(View.GONE);
                performSearch();
            }
        });
        globe.setBackgroundColor(ContextCompat.getColor(mThemeContext, android.R.color.transparent));
        search.setBackgroundColor(ContextCompat.getColor(mThemeContext, android.R.color.transparent));
        filter.setBackgroundColor(ContextCompat.getColor(mThemeContext, android.R.color.transparent));
        latin.setBackgroundColor(ContextCompat.getColor(mThemeContext, R.color.semi_transparent));
        return mCurrentInputView;
    }

    private void performSearch() {


        new SearchResultsGetTask().execute();
    }

    public void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) mThemeContext.getSystemService(Context.INPUT_METHOD_SERVICE);
//        View v = getCurrentFocus();
//        if (v != null)
            imm.showSoftInput(editText, 0);
    }

    public void setPager(final boolean isHardwareAcceleratedDrawingEnabled){

//        viewPager = (ViewPager) mCurrentInputView.findViewById(R.id.viewpager);
//        adapter =  new CustomPagerAdapter(mThemeContext);
//        viewPager.setAdapter(adapter);
//
//        //setTabListeners();
//
////        Handler handler = new Handler
//        mHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//            }
//        },300);
////
////
//        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//                View view = viewPager.getChildAt(position);
//                if(view == null){
//                    Log.d("viecheck","viewchild  null"+position);
//                    return;
//                }else{
//                    Log.d("viecheck","viewchild NOT null"+position);
//                }
//
//                if(position == 3){
//                    final RelativeLayout latinTabContent = (RelativeLayout) viewPager.getChildAt(3);
//                    if(latinTabContent == null){
//                        Log.d("viecheck","latinTabContent null");
//                        return;
//                    }else{
//                        Log.d("viecheck","latinTabContent NOT null");
//                    }
//                    mMainKeyboardFrame = latinTabContent.findViewById(R.id.main_keyboard_frame); //main_keyboard_frame //testView
//                    mEmojiPalettesView = (EmojiPalettesView)latinTabContent.findViewById(
//                            R.id.emoji_palettes_view);
//                    //////
//                    mKeyboardView = (MainKeyboardView) latinTabContent.findViewById(R.id.keyboard_view);
//                    mKeyboardView.setHardwareAcceleratedDrawingEnabled(isHardwareAcceleratedDrawingEnabled);
//                    mKeyboardView.setKeyboardActionListener(mLatinIME);
//                    mEmojiPalettesView.setHardwareAcceleratedDrawingEnabled(
//                            isHardwareAcceleratedDrawingEnabled);
//                    mEmojiPalettesView.setKeyboardActionListener(mLatinIME);
//                }
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });


    }

    public void showLoadingProgress(boolean val) {
//        if(getActivity() == null && isDetached()){
//            return;
//        }
        if(progress_layout != null) {
            if (val) {
                progress_layout.setVisibility(View.VISIBLE);
            } else {
                progress_layout.setVisibility(View.GONE);
            }
        }
    }

    public void setTabListeners(){
        globe.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switchTab(R.id.globe_parent_layout);
                return true;
            }
        });
        search.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switchTab(R.id.search_parent_layout);
                return true;
            }
        });
        filter.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switchTab(R.id.filter_parent_layout);
                return true;
            }
        });
        latin.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switchTab(R.id.latin_parent_layout);
                return true;
            }
        });
    }

    public void switchTab(int tabId){
        switch (tabId){
            case R.id.globe_parent_layout:
//                globe_parent_layout.setVisibility(View.VISIBLE);
//                search_parent_layout.setVisibility(View.GONE);
//                filter_parent_layout.setVisibility(View.GONE);
//                latin_parent_layout.setVisibility(View.GONE);
                mLatinIME.onCustomRequest(Constants.CUSTOM_CODE_SHOW_INPUT_METHOD_PICKER);
                mSearchView.setVisibility(View.GONE);
                tabView.setVisibility(View.VISIBLE);
                globe.setBackgroundColor(ContextCompat.getColor(mThemeContext, R.color.semi_transparent));
                search.setBackgroundColor(ContextCompat.getColor(mThemeContext, android.R.color.transparent));
                filter.setBackgroundColor(ContextCompat.getColor(mThemeContext, android.R.color.transparent));
                latin.setBackgroundColor(ContextCompat.getColor(mThemeContext, android.R.color.transparent));
                break;
            case R.id.search_parent_layout:
                editText.setFocusable(true);
                editText.requestFocus();
                editText.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
                showKeyboard();
                globe_parent_layout.setVisibility(View.GONE);
                search_parent_layout.setVisibility(View.GONE);
                filter_parent_layout.setVisibility(View.GONE);
                latin_parent_layout.setVisibility(View.VISIBLE);
                mSearchView.setVisibility(View.VISIBLE);
                tabView.setVisibility(View.GONE);
                globe.setBackgroundColor(ContextCompat.getColor(mThemeContext, android.R.color.transparent));
                search.setBackgroundColor(ContextCompat.getColor(mThemeContext, R.color.semi_transparent));
                filter.setBackgroundColor(ContextCompat.getColor(mThemeContext, android.R.color.transparent));
                latin.setBackgroundColor(ContextCompat.getColor(mThemeContext, android.R.color.transparent));
                break;
            case R.id.filter_parent_layout:
                globe_parent_layout.setVisibility(View.GONE);
                search_parent_layout.setVisibility(View.GONE);
                filter_parent_layout.setVisibility(View.VISIBLE);
                getFilters();
                latin_parent_layout.setVisibility(View.GONE);
                mSearchView.setVisibility(View.GONE);
                tabView.setVisibility(View.VISIBLE);
                globe.setBackgroundColor(ContextCompat.getColor(mThemeContext, android.R.color.transparent));
                search.setBackgroundColor(ContextCompat.getColor(mThemeContext, android.R.color.transparent));
                filter.setBackgroundColor(ContextCompat.getColor(mThemeContext, R.color.semi_transparent));
                latin.setBackgroundColor(ContextCompat.getColor(mThemeContext, android.R.color.transparent));
                break;
            case R.id.latin_parent_layout:
                globe_parent_layout.setVisibility(View.GONE);
                search_parent_layout.setVisibility(View.GONE);
                filter_parent_layout.setVisibility(View.GONE);
                latin_parent_layout.setVisibility(View.VISIBLE);
                mSearchView.setVisibility(View.GONE);
                tabView.setVisibility(View.VISIBLE);
                globe.setBackgroundColor(ContextCompat.getColor(mThemeContext, android.R.color.transparent));
                search.setBackgroundColor(ContextCompat.getColor(mThemeContext, android.R.color.transparent));
                filter.setBackgroundColor(ContextCompat.getColor(mThemeContext, android.R.color.transparent));
                latin.setBackgroundColor(ContextCompat.getColor(mThemeContext, R.color.semi_transparent));
                break;



        }

    }

    private void getFilters() {
        new FiltersGetTask().execute();
    }

    private class FiltersGetTask extends AsyncTask<Void, Void, Boolean> {
        FiltersCallbackListener callBackListener = new FiltersCallbackListener() {
            @Override
            public void onSuccess(ArrayList<String> filters) {
                data = filters;
                uiHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        showLoadingProgress(false);
                        initUI();
                    }
                });
            }

            @Override
            public void onFailure() {
                showLoadingProgress(false);
            }
        };
        @Override
        protected void onPreExecute() {
            showLoadingProgress(true);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            if(mThemeContext != null && mThemeContext.getApplicationContext() != null && AppUtils.isNetworkPresent(mThemeContext.getApplicationContext())) {
                HttpManager.getInstance().getFilters(callBackListener);
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean value) {
        }
    }

    private void initUI() {
//        if(search_parent_layout != null){
//            search_parent_layout.setVisibility(View.GONE);
//        }
//        if(filter_parent_layout != null){
//            filter_parent_layout.setVisibility(View.VISIBLE);
//        }
//        if(search_recyclerview != null){
//            search_recyclerview.setVisibility(View.GONE);
//        }
        filterAdapter = new FilterAdapter(data);
        if(filter_recyclerView != null) {
            filter_recyclerView.setVisibility(View.VISIBLE);
            filter_recyclerView.setAdapter(filterAdapter);
        }
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        if(mThemeContext != null && mThemeContext.getApplicationContext() != null) {
            Resources r = mThemeContext.getApplicationContext().getResources();
            return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
        }
        return -1;
    }

    private void initSearchTab(){
        editText.clearFocus();
        InputMethodManager in = (InputMethodManager)mThemeContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(editText.getWindowToken(), 0);
//        if(filter_recyclerView != null) {
//            filter_recyclerView.setVisibility(View.GONE);
//        }
        if(search_parent_layout != null){
            search_parent_layout.setVisibility(View.VISIBLE);
        }
        if(filter_parent_layout != null){
            filter_parent_layout.setVisibility(View.GONE);
        }

        searchResultsAdapter = new SearchResultsAdapter(stickersData, mThemeContext.getApplicationContext());
        if(search_recyclerview != null) {
            search_recyclerview.setVisibility(View.VISIBLE);
            search_recyclerview.setAdapter(searchResultsAdapter);
        }
    }

    private  class SearchResultsGetTask extends AsyncTask<Void, Void, Boolean> {
        SearchCallBackListener callBackListener = new SearchCallBackListener() {
            @Override
            public void onSuccess(ArrayList<Sticker> stickers) {
                stickersData = stickers;
                uiHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        showLoadingProgress(false);
                        initSearchTab();
                    }
                });
            }

            @Override
            public void onFailure() {
                showLoadingProgress(false);
            }
        };
        @Override
        protected void onPreExecute() {
            showLoadingProgress(true);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            if(mThemeContext != null && mThemeContext.getApplicationContext() != null && AppUtils.isNetworkPresent(mThemeContext.getApplicationContext())) {
                HttpManager.getInstance().getSearchResults(callBackListener);
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean value) {
        }

    }



    public int getKeyboardShiftMode() {
        final Keyboard keyboard = getKeyboard();
        if (keyboard == null) {
            return WordComposer.CAPS_MODE_OFF;
        }
        switch (keyboard.mId.mElementId) {
        case KeyboardId.ELEMENT_ALPHABET_SHIFT_LOCKED:
        case KeyboardId.ELEMENT_ALPHABET_SHIFT_LOCK_SHIFTED:
            return WordComposer.CAPS_MODE_MANUAL_SHIFT_LOCKED;
        case KeyboardId.ELEMENT_ALPHABET_MANUAL_SHIFTED:
            return WordComposer.CAPS_MODE_MANUAL_SHIFTED;
        case KeyboardId.ELEMENT_ALPHABET_AUTOMATIC_SHIFTED:
            return WordComposer.CAPS_MODE_AUTO_SHIFTED;
        default:
            return WordComposer.CAPS_MODE_OFF;
        }
    }

    public int getCurrentKeyboardScriptId() {
        if (null == mKeyboardLayoutSet) {
            return ScriptUtils.SCRIPT_UNKNOWN;
        }
        return mKeyboardLayoutSet.getScriptId();
    }
}
