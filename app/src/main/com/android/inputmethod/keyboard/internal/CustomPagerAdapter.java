package com.android.inputmethod.keyboard.internal;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.inputmethod.R;

/**
 * Created by anupamchugh on 26/12/15.
 */
public class CustomPagerAdapter extends PagerAdapter {

    private Context mContext;

    public CustomPagerAdapter(Context context) {
        mContext = context;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
       // ModelObject modelObject = ModelObject.values()[position];
        int layoutId = 0;
        switch (position){
            case 0:
                layoutId = R.layout.tab_globe_content;
            break;
            case 1:
                layoutId = R.layout.tab_search_content;
                break;
            case 2:
                layoutId = R.layout.tab_filter_content;
                break;
            case 3:
                layoutId = R.layout.tab_latin_content;
                break;

        }
        Log.d("viecheck","instantiateItem pos = "+position);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(layoutId, collection, false);//modelObject.getLayoutResId()
        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return 4;//ModelObject.values().length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
//        ModelObject customPagerEnum = ModelObject.values()[position];
//
//        return mContext.getString(customPagerEnum.getTitleResId());

        return "title"+position;
    }

}
