package com.android.inputmethod.listener;

import com.android.inputmethod.latin.http.model.Sticker;

import java.util.ArrayList;

/**
 * Created by bkasmoor on 05/04/18.
 */

public interface SearchCallBackListener {
    public void onSuccess(ArrayList<Sticker> stickers);

    public void onFailure();
}
