package com.android.inputmethod.listener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bkasmoor on 04/04/18.
 */

public interface FiltersCallbackListener {
    public void onSuccess(ArrayList<String> filters);

    public void onFailure();
}
