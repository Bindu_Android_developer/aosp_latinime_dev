package com.android.inputmethod.adapter;

/**
 * Created by bkasmoor on 30/03/18.
 */

public interface TabInterface {
    public void onTabSelected();
    public void onTabUnselected();
}
