package com.android.inputmethod.adapter;

/**
 * This is a helper class that implements the management of tabs and all
 * details of connecting a ViewPager with associated ActionBar. The TabsAdapter
 * listens to changes in tabs, and takes care of switch to the correct page in
 * the ViewPager whenever the selected tab changes.
 */

import android.graphics.drawable.Drawable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.Tab;
import android.support.design.widget.TabLayout.TabLayoutOnPageChangeListener;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class TabsAdapter extends FragmentStatePagerAdapter {
	private final FragmentActivity activity;
	private final TabLayout tabLayout;
	private final ViewPager mViewPager;
	private final PageChangeListener pageChangeListener;
	private final Map<String, TabInfo> tabInfoMap = new LinkedHashMap<String, TabInfo>();
	private final ArrayList<TabInfo> mTabs = new ArrayList<TabInfo>();
	
	private static class TabInfo {
		private final Class<?> clss;
		private Fragment fragment = null;

		private TabInfo(Class<?> clss) {
			this.clss = clss;
		}
	}

    public TabsAdapter(FragmentActivity activity, TabLayout tabLayout, ViewPager pager) {
        super(activity.getSupportFragmentManager());
		this.activity = activity;
		this.tabLayout = tabLayout;
		mViewPager = pager;
		mViewPager.setAdapter(this);
		this.pageChangeListener = new PageChangeListener(tabLayout);
    }

    public Tab getTabByTag(String tabTag) {
    	int tabCount = tabLayout.getTabCount();
    	
    	for (int i=0; i<tabCount; i++) {
    		if (tabLayout.getTabAt(i).getTag().equals(tabTag)) {
    			return tabLayout.getTabAt(i);
    		}
    	}
    	return null; 
	}

	public Fragment getItemByTag(String tabTag) {
		int position = mTabs.indexOf(tabInfoMap.get(tabTag));
		if (position == -1) return null;
		return getItem(position);
	}

	public void setCurrentTab(String tabTag) {
		TabInfo tabInfo = tabInfoMap.get(tabTag);
    	if (tabInfo != null) {
    		int tabIndex = mTabs.indexOf(tabInfo);

    		if (tabLayout.getSelectedTabPosition() != tabIndex) {
    			tabLayout.getTabAt(tabIndex).select();
    		}
    	}
	}

	public void setCurrentItem(int position) {
		if (mViewPager.getCurrentItem() != position) {
			mViewPager.setCurrentItem(position);
		}
	}

	public Map<String, TabInfo> getTabs() {
		return tabInfoMap;
	}

	public void addTab(String tag, Drawable drawable, Class<?> fragment) {
		TabInfo info = new TabInfo(fragment);
		tabInfoMap.put(tag, info);
		mTabs.add(info);
		notifyDataSetChanged();
	}

    public void setListeners() {
    	mViewPager.removeOnPageChangeListener(pageChangeListener);
        mViewPager.addOnPageChangeListener(pageChangeListener);
    }
    
	@Override
	public int getCount() {
		return tabInfoMap.size();
	}

	@Override
	public Fragment getItem(int position) {
        TabInfo info = mTabs.get(position);
        
        if (info.fragment == null) {
        	info.fragment = Fragment.instantiate(activity, info.clss.getName());
        }

        return info.fragment;
	}

	public void setInitialTab(String tag) {
		TabInfo tabInfo = tabInfoMap.get(tag);
    	if (tabInfo != null) {
    		tabLayout.getTabAt(mTabs.indexOf(tabInfo)).select();
    	}
	}

	//TODO TabLayout text color flickers when changing pages https://code.google.com/p/android/issues/detail?id=180454
	private class PageChangeListener extends TabLayoutOnPageChangeListener {
		private int mScrollState;
		private int mScrollPosition;
		private float mScrollOffset;
		private TabLayout tabLayout;
		
		public PageChangeListener(TabLayout tabLayout) {
			super(tabLayout);
			this.tabLayout = tabLayout;
		}
		
		@Override
		public void onPageScrollStateChanged(int state) {
			super.onPageScrollStateChanged(state);
			mScrollState = state;
		}

		@Override
		public void onPageScrolled(int position, float positionOffset,
		        int positionOffsetPixels) {
			super.onPageScrolled(position, positionOffset, positionOffsetPixels);
			mScrollPosition = position;
			mScrollOffset = positionOffset;
		}

		@Override
		public void onPageSelected(int position) {
			super.onPageSelected(position);
//			if (mScrollState != ViewPager.SCROLL_STATE_IDLE) {
//				tabLayout.setScrollPosition(mScrollPosition, mScrollOffset, true);
//			}
		}
	} 
}
