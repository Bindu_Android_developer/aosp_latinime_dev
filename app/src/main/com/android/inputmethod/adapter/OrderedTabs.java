package com.android.inputmethod.adapter;


public class OrderedTabs implements Comparable<OrderedTabs> {

	public static final String GLOBE_TAB_NAME = "globe";
	public static final String SEARCH_TAB_NAME = "search";
	public static final String FILTER_TAB_NAME = "filter";
	public static final String KEYPAD_TAB_NAME = "keypad";


	private String name;
	private int position = 100;

	public OrderedTabs(String name) {
		this.name = name;
	}

	@Override
	public int compareTo(OrderedTabs another) {
		if(this.position < another.position) {
			return -1;
		} else if (this.position > another.position) {
			return 1;
		} else {
			return 0;
		}
	}

	public String getName() {
		return name;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}
}
