package com.android.inputmethod.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.inputmethod.R;
import com.android.inputmethod.latin.http.model.Sticker;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by bkasmoor on 05/04/18.
 */

public class SearchResultsAdapter extends RecyclerView.Adapter<SearchResultsAdapter.ViewHolder>  {
    private ArrayList<Sticker> stickers;
    private Context mContext;

    public SearchResultsAdapter(ArrayList<Sticker> stickers, Context context) {
        this.stickers = stickers;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.search_result_view, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Picasso.with(mContext).load(stickers.get(i).getImagepath()).resize(30, 30).into(viewHolder.sticker);

//        Picasso.with(mContext).load(stickers.get(i).getImagepath())
//                .fit()
//                .centerCrop()
//                .into(viewHolder.sticker);

    }

    @Override
    public int getItemCount() {
        return stickers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView sticker;
        public ViewHolder(View view) {
            super(view);

            sticker = (ImageView) view.findViewById(R.id.sticker);

        }
    }
}
