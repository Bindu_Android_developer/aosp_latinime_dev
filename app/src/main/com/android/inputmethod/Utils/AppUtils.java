package com.android.inputmethod.Utils;

import android.content.Context;
import android.net.NetworkInfo;

import com.android.inputmethod.Manager.NetworkManager;

/**
 * Created by bkasmoor on 04/04/18.
 */

public class AppUtils {
    private static final String TAG = AppUtils.class.getSimpleName();

    public static boolean isNetworkPresent(Context context){
        if(context != null) {
            NetworkInfo currentNetworkInfo = null;
            currentNetworkInfo = NetworkManager.getInstance().getAvailableNetwork(context);
            if (currentNetworkInfo != null && currentNetworkInfo.isConnected()) {
                return true;
            }
        }
        return false;
    }

}
