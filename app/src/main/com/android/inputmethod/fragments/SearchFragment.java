package com.android.inputmethod.fragments;

import android.content.res.Resources;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.android.inputmethod.R;
import com.android.inputmethod.Utils.AppUtils;
import com.android.inputmethod.adapter.FilterAdapter;
import com.android.inputmethod.adapter.SearchResultsAdapter;
import com.android.inputmethod.adapter.TabInterface;
import com.android.inputmethod.latin.http.HttpManager;
import com.android.inputmethod.latin.http.model.Sticker;
import com.android.inputmethod.listener.FiltersCallbackListener;
import com.android.inputmethod.listener.SearchCallBackListener;

import java.util.ArrayList;

/**
 * Created by bkasmoor on 30/03/18.
 */

public class SearchFragment extends Fragment implements TabInterface {
    private RecyclerView recyclerView;
    private ArrayList<Sticker> data;
    private SearchResultsAdapter adapter;
    private RelativeLayout progress_layout;
    private Handler uiHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.search_query_list, container, false);
        initViews(view, savedInstanceState);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
    private void initViews(View view, Bundle savedInstanceState){
        recyclerView = (RecyclerView)view.findViewById(R.id.search_recycler_view);
        if(getActivity() != null) {
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.addItemDecoration(new GridSpacingItemDecoration(3, dpToPx(10), true));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
        }
        progress_layout = (RelativeLayout) view.findViewById(R.id.progress_layout);
        progress_layout.setVisibility(View.GONE);
        progress_layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        uiHandler = new Handler();

    }

    public void showLoadingProgress(boolean val) {
        if(getActivity() == null && isDetached()){
            return;
        }
        if(progress_layout != null) {
            if (val) {
                progress_layout.setVisibility(View.VISIBLE);
            } else {
                progress_layout.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onTabSelected() {
        getSearchData();
    }

    private void getSearchData() {
        new SearchResultsGetTask().execute();
    }

    @Override
    public void onTabUnselected() {

    }

    private  class SearchResultsGetTask extends AsyncTask<Void, Void, Boolean> {
        SearchCallBackListener callBackListener = new SearchCallBackListener() {
            @Override
            public void onSuccess(ArrayList<Sticker> stickers) {
                data = stickers;
                uiHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        showLoadingProgress(false);
                        initUI();
                    }
                });
            }

            @Override
            public void onFailure() {
                showLoadingProgress(false);
            }
        };
        @Override
        protected void onPreExecute() {
            showLoadingProgress(true);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            if(getActivity() != null && AppUtils.isNetworkPresent(getActivity())) {
               HttpManager.getInstance().getSearchResults(callBackListener);
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean value) {
        }

    }

    private void initUI() {
        adapter = new SearchResultsAdapter(data, getActivity());
        recyclerView.setAdapter(adapter);
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}